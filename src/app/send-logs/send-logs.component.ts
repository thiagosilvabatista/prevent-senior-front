import { environment } from './../../environments/environment';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FileUploader, FileItem } from 'ng2-file-upload';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { Router } from '@angular/router';

const URL = 'http://localhost:8082/api/log-file/import'

@Component({
  selector: 'app-send-logs',
  templateUrl: './send-logs.component.html',
  styleUrls: ['./send-logs.component.css']
})
export class SendLogsComponent implements OnInit {

  uploader:FileUploader;
  hasBaseDropZoneOver:boolean;
  hasAnotherDropZoneOver:boolean;
  response:string;
  displayedColumns: string[] = ['name', 'size', 'send'];
  dataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private router: Router) { 

    this.uploader = new FileUploader({
      url: URL,
      formatDataFunction: async (item) => {
        return new Promise( (resolve, reject) => {
          resolve({
            name: item._file.name,
            length: item._file.size,
            contentType: item._file.type,
            date: new Date()
          });
        });
      }
    });    
    this.hasBaseDropZoneOver = false;
    this.hasAnotherDropZoneOver = false;
 
    this.response = '';
 
    this.uploader.response.subscribe( res => this.response = res );

   }

  ngOnInit(): void {

    this.uploader.onAfterAddingFile = f => {
        if (this.uploader.queue.length > 1) {
            this.uploader.removeFromQueue(this.uploader.queue[0]);
        }
    };

  }

  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
    this.dataSource = new MatTableDataSource<any>(this.uploader.queue);
  }
 
  public fileOverAnother(e:any):void {
    this.hasAnotherDropZoneOver = e;
  }

  home() {
    this.router.navigate(['']);
  }

  upload(item: FileItem){
    item.upload();
  }

}
