import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendLogsComponent } from './send-logs.component';

describe('SendLogsComponent', () => {
  let component: SendLogsComponent;
  let fixture: ComponentFixture<SendLogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendLogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
