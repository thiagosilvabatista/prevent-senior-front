import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(private http: HttpClient) { }

  getLogsFilter(ip: string[], startDate: string[], endDate: string[]){    
    let parameters = '';

    if(ip != null && ip[0]){
      parameters += "&strIp="+ ip.join(',')
    }
    if(startDate != null && startDate[0]){
      parameters += "&startDate=" + startDate.join(',');
    }

    if(endDate != null && endDate[0]){
      parameters += "&endDate=" + endDate.join(',');
    }   

    return this.http.get<any>(
      environment.api + 'log-file/filter?' + parameters
    );
  }

  saveLogFile(logFile: any){
    return this.http.post(environment.api + 'log-file/', logFile);    
  }

  deleteLog(numId: any) {    
    return this.http.delete(environment.api + `log-file/${numId}`, {responseType: 'text'});
  }

  updateLogFile(logFile: any) {    
    return this.http.put(environment.api + 'log-file/', logFile); 
  }

  findLogById(numId: any) {    
    return this.http.get<any>(
      environment.api + `log-file/${numId}`
    );
  }

  findViewGroupByHourByIp(ip: any) {    
    return this.http.get<any>(
      environment.api + `log-file/group-by-hour?strIp=${ip}`
    );
  }

}
