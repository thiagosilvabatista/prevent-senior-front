import { EditLogsComponent } from './edit-logs/edit-logs.component';
import { SendLogsComponent } from './send-logs/send-logs.component';
import { InsertLogsComponent } from './insert-logs/insert-logs.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'insert-logs', component: InsertLogsComponent},
  { path: 'send-logs', component: SendLogsComponent},
  { path: 'edit-logs/:numId', component: EditLogsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
