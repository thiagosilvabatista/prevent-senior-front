import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertLogsComponent } from './insert-logs.component';

describe('InsertLogsComponent', () => {
  let component: InsertLogsComponent;
  let fixture: ComponentFixture<InsertLogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertLogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
