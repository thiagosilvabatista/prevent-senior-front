import { DialogComponent } from './../share/dialog/dialog.component';
import { LogService } from './../services/log.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup,  Validators } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-insert-logs',
  templateUrl: './insert-logs.component.html',
  styleUrls: ['./insert-logs.component.css']
})
export class InsertLogsComponent implements OnInit {

  subscriptionForm: FormGroup;  

  constructor(private fb: FormBuilder, 
              private logService: LogService,
              public dialog: MatDialog, 
              private router: Router) { 

    this.subscriptionForm = fb.group({
      ip: [''],
      request: [''],
      status: [''],
      userAgent: [''],
    });

   }

  ngOnInit(): void {
  }

  insert() {
    if(this.fieldValidator(this.subscriptionForm.value)){
      let logFile = {
        numStatus: this.subscriptionForm.value.status,
        strIp: this.subscriptionForm.value.ip,
        strRequest: this.subscriptionForm.value.request,
        strUserAgent: this.subscriptionForm.value.userAgent
      };

      this.logService.saveLogFile(logFile).subscribe(data => {
          this.openDialog('Log inserido com sucesso.');
          this.router.navigate(['']);        
        }, erro => {        
          this.openDialog('Ocorreu um erro ao salvar o log, por favor verifique se os campos estão preenchidos corretamente.');
        });
    }

  }

  openDialog(message: string): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '350px',
      data: message
    });
  }

  fieldValidator(fileld) {
    if(fileld.status === '' || fileld.ip === '' ||
       fileld.request === '' || fileld.userAgent === '') {
         this.openDialog('Todos os campos são obrigatórios.');
         return false;
       }
       return true;
      
  }

  home() {
    this.router.navigate(['']);
  }

}
