import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-options',
  templateUrl: './dialog-options.component.html',
  styleUrls: ['./dialog-options.component.css']
})
export class DialogOptionsComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DialogOptionsComponent>, 
              @Inject(MAT_DIALOG_DATA) public message: string) { }

  ngOnInit() {}

   onNoClick(): void {
    this.dialogRef.close();
  }

}
