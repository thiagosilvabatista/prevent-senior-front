import { DialogComponent } from './../share/dialog/dialog.component';
import { DialogOptionsComponent } from './../share/dialog-options/dialog-options.component';
import { LogService } from './../services/log.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup,  Validators } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
//import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';
import * as _ from 'lodash';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  subscriptionForm: FormGroup;
  public maskDate = [/\d/, /\d/, /\d/, /\d/, '-' ,/\d/, /\d/, '-', /\d/, /\d/];
  displayedColumns: string[] = ['id', 'ip', 'request', 'status', 'userAgent','edit', 'delete'];
  dataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  logData: any;
  graphData: any;

  public barChartOptions: ChartOptions = {
    responsive: true,    
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };

  public barChartLabels: Label[];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;  
  public barChartData: ChartDataSets[];
  
  constructor(private fb: FormBuilder, 
              private logService: LogService,
              public dialog: MatDialog, 
              private router: Router) { 

    this.subscriptionForm = fb.group({
      logIp: [''],
      startDate: [''],
      endDate: ['']
    }); 


   }

  ngOnInit(): void {}

  
  fieldValidator() {
    if(this.subscriptionForm.value.logIp !== '' &&
       this.subscriptionForm.value.startDate !== '' &&
       this.subscriptionForm.value.endDate !== '') {

         return true

       }
       return false;
  }

  search() {
    if(this.fieldValidator()){
      let searchData = this.subscriptionForm.value;

      let logIp = [];
      let startDate = [];
      let endDate = [];
      
      if(searchData.logIp && searchData.logIp !== '') {
        logIp.push([searchData.logIp.split('_')[0]]);
      }

      if(searchData.startDate && searchData.startDate !== '') {
        startDate.push([searchData.startDate + ' 00:00:00.000']);

      }

      if(searchData.endDate && searchData.endDate !== '') {
        endDate.push([searchData.endDate + ' 23:59:59.999']);
      }

      this.logService.getLogsFilter(logIp, startDate, endDate).subscribe(data => {
        this.logData = data;
        this.dataSource = new MatTableDataSource<any>(this.logData);
        this.dataSource.paginator = this.paginator;
        if(this.logData && this.logData.length > 0) {
          this.findViewGroupByHourByIp(logIp[0]);
        } else {
          this.barChartLabels = [];
          this.barChartData = [];
          this.graphData =[];
          this.openDialogMessage('Nenhum registro encontrado.');
        }
      });
    } else {
       this.openDialogMessage('Campos IP, Data Inical e Data Final são obrigatórios');
    }
  }

  insertLog() {
    this.router.navigate(['/insert-logs']);
  }

  sendLogs() {
    this.router.navigate(['/send-logs']);
  }

  edit(numId: any) {
    this.router.navigate(['/edit-logs/'+numId]);
  }

  delete(numId: any) {
    this.openDialogOptions(`Deseja deletar o log ${numId}`, numId);
  }

  openDialogOptions(messageErro: string, numId): void {
    const dialogRef = this.dialog.open(DialogOptionsComponent, {
      width: '350px',
      data: messageErro
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.logService.deleteLog(numId).subscribe(data => {
          this.openDialogMessage('Log deletado com sucesso.');
          this.search();     
        }, erro => {        
          this.openDialogMessage(erro.message);
          this.search();
        });           
      }
    })
  } 

  openDialogMessage(messageErro: string): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '350px',
      data: messageErro
    });
  }

  public randomize(): void {
    const data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    this.barChartData[0].data = data;
  }

  findViewGroupByHourByIp(ip: string) {
    this.logService.findViewGroupByHourByIp(ip).subscribe(data => {
    this.graphData = _.orderBy(data, ['dateDate'], ['asc']);        
    this.barChartLabels = this.graphData.map(data => data.dateDate);
    this.barChartData = [{ data: this.graphData.map(data => data.numCount), 
                           label: this.graphData[0].strIp }];
    })
  }

}
