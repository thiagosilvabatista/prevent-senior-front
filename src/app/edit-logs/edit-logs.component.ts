import { DialogComponent } from './../share/dialog/dialog.component';
import { LogService } from './../services/log.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup,  Validators } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-logs',
  templateUrl: './edit-logs.component.html',
  styleUrls: ['./edit-logs.component.css']
})
export class EditLogsComponent implements OnInit {

  subscriptionForm: FormGroup;
  public maskIp = [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/];

  numId: any;
  log = <any> {};

  constructor(private fb: FormBuilder, 
              private logService: LogService,
              public dialog: MatDialog, 
              private router: Router,
              private activatedRoute: ActivatedRoute) { 

    this.numId = activatedRoute.snapshot.paramMap.get('numId');
    this.getLogById(this.numId);

    this.subscriptionForm = fb.group({
      ip: [''],
      request: [''],
      status: [''],
      userAgent: [''],
      dateData: ['']
    });

  }

  ngOnInit() {
  }

  getLogById(numId: number){
    this.logService.findLogById(numId).subscribe(data => {
      this.log.numId= data['numId'];
      this.log.dateData= data['dateData'];
      this.log.numStatus= data['numStatus'];
      this.log.strIp= data['strIp'];
      this.log.strRequest= data['strRequest'];
      this.log.strUserAgent= data['strUserAgent'];

      this.subscriptionForm.controls['dateData'].setValue(this.log.dateData);
      this.subscriptionForm.controls['status'].setValue(this.log.numStatus);
      this.subscriptionForm.controls['ip'].setValue(this.log.strIp);
      this.subscriptionForm.controls['request'].setValue(this.log.strRequest);
      this.subscriptionForm.controls['userAgent'].setValue(this.log.strUserAgent);
    });
  }

  update() {
    if(this.fieldValidator(this.subscriptionForm.value)) {
      let logFile = {
        numId: this.numId,
        numStatus: this.subscriptionForm.value.status,
        strIp: this.subscriptionForm.value.ip,
        strRequest: this.subscriptionForm.value.request,
        strUserAgent: this.subscriptionForm.value.userAgent,
        dateData: this.subscriptionForm.value.dateData
      };

      this.logService.updateLogFile(logFile).subscribe(data => {
          this.openDialog('Log atualizado com sucesso.');
          this.router.navigate(['']);        
        }, erro => {        
          this.openDialog('Ocorreu um erro ao atualizar o log, por favor verificar se os campos estão preenchidos corretamente.');
        });
    }
  }

  openDialog(message: string): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '350px',
      data: message
    });
  }

  fieldValidator(fileld) {
    if(fileld.status === '' || fileld.ip === '' ||
       fileld.request === '' || fileld.userAgent === '') {
         this.openDialog('Todos os campos são obrigatórios.');
         return false;
       }
       return true;
      
  }

  home() {
    this.router.navigate(['']);
  }
  

}
